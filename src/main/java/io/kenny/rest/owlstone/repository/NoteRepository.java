package io.kenny.rest.owlstone.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.kenny.rest.owlstone.model.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long>{
	
}
