package io.kenny.rest.owlstone.restcontroller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.kenny.rest.owlstone.model.Note;
import io.kenny.rest.owlstone.service.NoteService;

@RestController
@RequestMapping("/api")
public class NoteApiController {
	
	private final Logger logger = LoggerFactory.getLogger(NoteApiController.class);
	
	@Autowired
	private NoteService noteService;

    @RequestMapping("/note")
    public Page<Note> getNotes(@RequestParam(name="r", required=false) boolean refresh, Pageable pageable) {
    	
    	if(refresh){
    		logger.info("Apply refresh to cache notes!");
    		noteService.refreshAllNotes();
    	}
    	
    	Page<Note> notes = noteService.findAll(pageable);
        return notes;
    }
    
    @RequestMapping("/note/{id}")
    public Note getNote(@PathVariable Long id){
    	return noteService.findOne(id);
    }

    @RequestMapping(value="/note",  method = RequestMethod.POST)
    public Note saveNote(@RequestBody Note note) {
    
    	noteService.saveAndUpdate(note);
    	return note;
    }

    @RequestMapping(value="/note", method = RequestMethod.PUT)
    public Note updateNote(@RequestBody Note note){
    	
    	noteService.saveAndUpdate(note);
    	
    	return note;
    }

    @RequestMapping(value="/note/{id}", method = RequestMethod.DELETE)
    public String deleteNote(@PathVariable Long id){
    	noteService.delete(id);
    	return "Note deleted!";
    }
}