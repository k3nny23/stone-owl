package io.kenny.rest.owlstone.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import io.kenny.rest.owlstone.model.Note;
import io.kenny.rest.owlstone.repository.NoteRepository;

@Service
public class NoteService {
	
	private final Logger logger = LoggerFactory.getLogger(NoteService.class);
	private StringBuilder msgLogger;

	@Autowired
	private NoteRepository noteRepository;
	
	/**
	 * Get all notes with paginable.
	 * @param pageable - enable the pagination.
	 * @return - one list of notes with pagination.
	 */
	@Cacheable("notes")
	public Page<Note> findAll(Pageable pageable){
		logger.info("Finding all notes");
		return noteRepository.findAll(pageable);
	}
	
	/**
	 * This method will remove all 'notes' from cache, say as a result of flush API call
	 */
	@CacheEvict(value="notes", allEntries = true)
	public void refreshAllNotes(){
		logger.info("refreshing cache 'notes'");
	}
	
	/**
	 * Get one note by id.
	 * @param id - id of note
	 * @return - return one note if it exist in database.
	 */
	public Note findOne(Long id){
		msgLogger = new StringBuilder("Finding note with id ").append(id);
		logger.info(msgLogger.toString());
		return noteRepository.findOne(id);
	}
	
	/**
	 * Save or Update one note.
	 * @param note - save or update one note, it depend if that note exist in database.
	 * @return - return the note is saved or updated.
	 */
	public Note saveAndUpdate(Note note) {
		msgLogger = new StringBuilder();
		
		if(note.getId() == null){
			msgLogger.append("Saving ");
		}else{
			msgLogger.append("Updating ");
		}
		logger.info(msgLogger.append(note).toString());
		
//    	That is a good ideia? refresh caching here
//    	refreshAllNotes();
		
		return noteRepository.save(note);
	}
	
	/**
	 * Delete one note.
	 * @param id
	 */
	public void delete(Long id){
		msgLogger = new StringBuilder("Deleting note with id ").append(id);
		logger.info(msgLogger.toString());
		noteRepository.delete(id);
	}
}
