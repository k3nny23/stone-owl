package io.kenny.rest.owlstone.config.cache;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableCaching
public class CacheConfig {
	@Bean
	public CacheManager cacheManager(){
		return new EhCacheCacheManager(ehCacheCacheManager().getObject());
	}

	@Bean
	public EhCacheManagerFactoryBean ehCacheCacheManager(){
		EhCacheManagerFactoryBean cacheFactory = new EhCacheManagerFactoryBean();
		cacheFactory.setConfigLocation(new ClassPathResource("ehcache.xml"));
		cacheFactory.setShared(true);
		return cacheFactory;
	}
}
